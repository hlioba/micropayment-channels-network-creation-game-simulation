# -*- coding: utf-8 -*-
"""
Created on Mon May 20 11:14:45 2019

@author: lioba
"""

"""import libraries"""
import networkx as nx
import numpy as np
import itertools
import copy
import matplotlib.pyplot as plt
import matplotlib as mpl
from time import time


class nashEquilibria: 
    
    def __init__(self, n,  b, c):
        
        #number of nodes
        self.n = n
        
        #raise expection if n is not an integer
        if isinstance(n, int) == 0: 
            raise Exception('n has to be integer')
            
        #raise expection if n is not at least two
        if n < 2: 
            raise Exception('n has to be larger than two')
        
        #betweenness weight
        self.b = b
        
        #raise expection if b < 0
        if b < 0: 
            raise Exception('b has to be non-negative')
        
        #closeness weight
        self.c = c
        
        #raise expection if c =< 0
        if c <= 0: 
            raise Exception('c has to be positive')
        
        #cost when a node not reachable
        self.inf = max((self.n**self.n),10000)    
        
    
    def calculateCost(self, ID, channelCreated): 
        """"
        This function calculates the cost for a node with the current setup.
        
        :param ID int: The id of the node in question. 
        :param channelCreated bool: Array indicating which channels are created.
        
        :return cost float: Cost encountered by ID. 
        :return G graph: The current graph. 
        """ 
        
        #make a local copy of channelCreated
        channelCreatedLocal = copy.deepcopy(channelCreated)
    
        #create grapth from channelCreatedLocal
        G=nx.from_numpy_matrix(channelCreatedLocal)
        
        #calculate the number of channels created by ID
        numberOfCreatedChannels = 0
        for i in range(0,self.n):
            numberOfCreatedChannels = numberOfCreatedChannels + channelCreatedLocal[ID,i]
            
        #initialize betweenness
        betweenness = 0
            
        for i in range(0,self.n): 
            if i == ID : 
                pass
            else: 
                for j in range(0,i):
                    if j == ID: 
                        pass
                    else:                         
                        #counts number of shortest paths
                        countAll = 0
                        
                        #counts number of shortest paths via ID
                        countPass = 0
                            
                        #find and count shortest paths
                        try:
                            for p in nx.all_shortest_paths(G, i, j, ): 
                                countAll = countAll + 1
                                
                                if ID in p: 
                                 
                                    countPass = countPass + 1
                                
                            #update betweenness
                            if countAll == 0: 
                                pass
                            else: 
                                betweenness = betweenness + 2*float(countPass)/float(countAll)
                                
                        except nx.NetworkXNoPath:
                            pass
        
        betweenness = self.n *(self.n -1)*(self.n-2) - betweenness                   
        
        #initialize closeness
        closeness = 0
            
        for i in range(0, self.n): 
            #calculate length of shortest path
            if i != ID: 
                
                try:
                    spLength = nx.shortest_path_length(G, ID, i) 
                    
                except nx.NetworkXNoPath: 
                    spLength = self.inf
                       
                #update closeness
                closeness = closeness + spLength -1
        
        #cost of node ID with current graph
        cost = int(numberOfCreatedChannels) + closeness*self.c + betweenness*self.b
        
        #return cost
        return (cost)
    
    
    def minCost(self, ID, startCost, channelCreated):
        """
        This function checks if current assignement leads to min cost of ID, 
        given the choices of the other nodes. 
        
        :param ID int: The id of node in quesiton. 
        :param startCost float: The current cost of ID. 
        :param channelCreated bool: Array indicating which channels are created.
        
        :return change bool: Indicates whether ID would change strategy. 
        """
      
        #indicates whether the graph would changee
        change = 0        
        
        #local created channel array
        channelCreatedLocal = copy.deepcopy(channelCreated)
        
        #array of all vertices except for current ID
        vertices = []
        for i in range(0,self.n): 
            if i != ID: 
                vertices.append(i)        
        
        for i in range(0,self.n):
            #iterate through combinations if i channels created
            for c in itertools.combinations(vertices, i):
                channelCreatedLocal[ID,:] = 0
                for v in c: 
                    channelCreatedLocal[ID,v] = 1     
               
                #initialize array indicating which node directely conntected to 
                #node ID                
                channels = np.zeros(self.n, dtype = int)
                
                #fill channels
                for j in range(0,self.n): 
                    if channelCreatedLocal[ID, j] == 1 or channelCreatedLocal[j,ID] == 1:
                        channels[j] = 1            
                       
                #calculate cost                  
                (cost) = self.calculateCost(ID, channelCreatedLocal)
                
                #if cost lower than startCost, no nash equilibirum, else continue
                #avoid floating point operation uncertainties
                if cost < (startCost -0.001*min(self.b/self.n, self.c/self.n, 0.000001)):
                    change = 1
                    
                    return (change)
                
                else: 
                    pass
     
        return change
    
    
    def iterateGraph(self):
        """
        This function iterates through all possible simple graphs for n nodes and 
        returns all nash equilibira found.
        
        :return nashEquilibria bool: Array of all nashEquilibira found.     
        """
        
        #record start time
        start = time()

        #initialize created channels array
        channelCreated = np.zeros((self.n,self.n), dtype = int)
    
        #initialize list for nashEquilibria
        nashEquilibria = []        
        
        #Initialize list indicating which vertex each vertex can link to, we have 
        #that vertices[ID] = [n-1]/ID. 
        vertices = []
        
        for ID in range(self.n):
            vertices.append([])
            for i in range(0,self.n): 
                if i != ID: 
                    vertices[ID].append(i)        
        
        #Initialize list indicating which combination of links is possible for 
        #each vertex, we have that combinations[ID] = list of possible link 
        #combinations. 
        combinations = []
        
        for ID in range(self.n): 
            combinations.append([])
            for channels in range(self.n):
                for c in itertools.combinations(vertices[ID], channels):
                    combinations[ID].append(c)
                    
        #iterate through all possible simple graphs
        for x in itertools.product(*combinations): 
            for i in range(self.n):
                channelCreated[i,:] = 0
                for v in x[i]: 
                    channelCreated[i,v] = 1
            
           #initialize nash varible, nash =1 if we have a nash equilibrium
            nash = 1
            
            #check if we have a Nash equilibirum
            for i in range(0,self.n):
                currMinCost =self.calculateCost(i, channelCreated)
                change = self.minCost(i, currMinCost, channelCreated)
                if change == 1: 
                    nash = 0
            
            #save Nash equilibrium
            if nash == 1:
                nashEquilibria.append(copy.deepcopy(channelCreated))
                
        #record end time
        end = time()
        
        #print the time taken
        print("Time taken to find Nash Equilibria: "+str(end-start) + " s")
        
        return nashEquilibria
    
    
    def initializeGame(self): 
        """
        This function randomly generates an adjacency matrix for the game.
        
        :return channelCreated bool: Array indicating which channels are created.
        """
        
        #initialize channelCreated
        channelCreated = np.random.randint(2, size=(self.n,self.n))
        
        return channelCreated
    
    
    def findBestStrategy(self, ID, startCost, channelCreated):
        """
        This function finds assignement for min cost of ID, given the 
        choices of the other nodes. 
        
        :param ID int: The id of node in quesiton. 
        :param startCost float: The current cost of ID. 
        :param channelCreated bool: Array indicating which channels are created.
        
        :return change bool: Indicates whether change occured. 
        :return bestChannel bool: Array indicating which channels are created 
        with ID's best response.
        """
        
        #indicates whether the graph would changee
        change = 0  
        
        #local created channel array
        channelCreatedLocal = copy.deepcopy(channelCreated)
        bestChannel = copy.deepcopy(channelCreated)
        
        #array of all vertices except for current ID
        vertices = []
        for i in range(0,self.n): 
            if i != ID: 
                vertices.append(i)        
        
        for i in range(0,self.n):
            #iterate through combinations if i channels created
            for c in itertools.combinations(vertices, i):
                channelCreatedLocal[ID,:] = 0
                for v in c: 
                    channelCreatedLocal[ID,v] = 1     
               
                #initialize array indicating which node directely conntected to 
                #node ID                
                channels = np.zeros(self.n, dtype = int)
                
                #fill channels
                for j in range(0,self.n): 
                    if channelCreatedLocal[ID, j] == 1 or channelCreatedLocal[j,ID] == 1:
                        channels[j] = 1            
                       
                #calculate cost                  
                (cost) = self.calculateCost(ID, channelCreatedLocal)
                
                #if cost lower than startCost, no nash equilibirum, else continue
                #avoid floating point operation uncertainties
                if cost < (startCost -0.001*min(self.b/self.n, self.c/self.n, 0.000001)):
                    change = 1
                    bestChannel = copy.deepcopy(channelCreatedLocal)
                
                else: 
                    pass
     
        return change, bestChannel
    
    
    def simulateGame(self, channelCreated): 
        """
        This function simulates the creation game given an initial graph and
        returns the Nash equilibrium reached. 
        
        :param channelCreated bool: Array indicating which channels are created.
        
        :return channelCreated bool: Array indicating which channels are created.
        """
        
        #initialize nash bool
        nash = 0
        
        while nash == 0: 
            nash = 1
            
            #iterate through nodes
            for ID in range(self.n):               
                
                #check if node ID would change strategy
                currMinCost =self.calculateCost(ID, channelCreated)
                change, bestChannel = self.findBestStrategy(ID, currMinCost, channelCreated)
                
                #update parameters if chnage occured
                if change == 1: 
                    nash = 0
                    channelCreated = bestChannel
                    pass 
            
        #return Nash equilibrium graph
        return channelCreated
    
    
    def drawGraphDirected(self, A, count): 
        """
        This function draws a directed graph for adjacency matrix A of a 
        Nash Equilibrium. 
        
        :param A bool: Adjacenty array of a directed graph. 
        :param count int: The count of the current Nash Equilibrium, used only 
        for differentiation. 
        """
        
        #define graph
        G=nx.from_numpy_matrix(A,create_using=nx.MultiDiGraph())
        
        #draw graph
        pos = nx.circular_layout(G)        
        nx.draw_networkx_nodes(G,pos,node_size=700, node_color = "#58AFD0")
        nx.draw_networkx_edges(G,pos,node_size=700,arrowstyle="->",arrowsize=10)
        nx.draw_networkx_labels(G,pos)
     
        #graph description
        plt.suptitle("Nash Equilibrium "+str(count)+" for "+str(self.n)+" Players", y= 1, fontsize =14)      
        plt.title("b="+str(self.b)+" and c ="+str(self.c), fontsize =10)
        
        #remove axis
        plt.axis('off')
        
        #name of png
        name = "n="+str(self.n)+"b"+str(self.b)+"c"+str(self.c)+"count"+str(count)+".png"
        
        #save plot
        plt.savefig(name)
        
        #show plot
        plt.show()
        
        return None
    
    
        
    def drawNashEqGraph(self):
        """
        This function draws all Nash Equilibira for the specified parameters. 
        """
        
        #record start time
        start = time()
        
        #find Nash equilibria
        nashEquilibria = self.iterateGraph()
        
        #start counter
        count = 1
        
        #draw graphs
        for i in nashEquilibria: 
            self.drawGraphDirected(i, count)
            count = count +1 
            
        #record end time
        end = time()
        
        #print the time taken
        print("Time taken to draw Nash Equilibria: "+str(end-start) + " s")
        
        return None
        
        
    def socialCost(self, channelCreated):
        """
        This function calculates the social cost. 
        
        :param channelCreated bool: Array indicating which channels are created.
        
        :return socialCost float: Social cost of setup.        
        """
        
        #initialize social cost
        socialCost = 0
        
        
        #calculate social cost, by adding over cost of all nodes
        for ID in range(self.n): 
            socialCost = socialCost + self.calculateCost(ID, channelCreated)
            
        return socialCost
    
    
    def isNash(self, channelCreated): 
        """
        This function determines if the graph 
        
        :param channelCreated bool: Array indicating which channels are created.  
        
        :return nash bool: Indicates if channelCreated represents a Nash Eq. 
                           (nash = 1 <=> channelCreated Nash Eq)
        """
        
        #iterate throuh all nodes
        for ID in range(self.n): 
            
            #check if node ID would change strategy
            currMinCost =self.calculateCost(ID, channelCreated)
            change = self.minCost(ID, currMinCost, channelCreated)
            
            if change == 1:
                nash = 0
                return nash
        
        #no node changed strategy, channelCreated represents Nash Eq
        nash = 1
        
        return nash
    
    
    def saveNash(self): 
        """
        This function saves all Nash Equilibria in an npz file. 
        """
        
        #find all Nash Eq
        nashEquilibria = self.iterateGraph()
        
        #save Nash Eq
        name = "n"+str(self.n)+"_b"+str(self.b)+"_c"+str(self.c)   
        np.savez(name, *[nashEquilibria[x] for x in range(len(nashEquilibria))])
        
    
    def loadNash(self, fileName): 
        """
        This function loads and draws graphs from numpy arrays provided in an 
        npz file and draws the corresponding Nash equilibria. 
        
        :param fileName string: The file name.
        """
        
        #load data
        nashData = np.load(fileName)
        
        #initialize counter
        count = 1
        
        #draw graph from data
        for i in nashData: 
            self.drawGraphDirected(nashData[i], count)
            count= count +1


def line_intersection(line1, line2):
    """
    This function computes the point of intersection of line 1 and line 2. 
    
    :param line1 float: line1 = [[x11,x12],[y11,y12]]
    :param line2 float: line2 = [[x21,x22],[y21,y22]]
    
    :return x float: x coordinate of intersection between line1 and line 2
    :return y float: y coordinate of intersection between line1 and line 2
    
    Taken from: https://stackoverflow.com/questions/20677795/how-do-i-compute-the-intersection-point-of-two-lines-in-python
    """
    
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
       raise Exception('lines do not intersect')

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    
    return x, y

    
def parameterSweepCircle(n): 
    """
    This function performs a parameter sweep for b and c to see when the 
    circle graph is a Nash equilibrium. 
    
    :param n int: The number of nodes in the network. 
    """
    
    #raise expection if n ist not an integer
    if isinstance(n, int) == 0: 
        raise Exception('n has to be integer')
        
    #ensure that the game has at least 2 players
    if n <= 1:
        raise Exception('n has to be at least 2')
    
    #initialize channelCreated array
    #Note: Circle Graph is a Nash equilibrium for a greater area, if every node 
    #has exactly one outgoing edge. Thus, initiate such a circle graph. 
    channelCreated = np.zeros((n,n))
    for i in range(n-1):
        channelCreated[i, i+1] = 1
    
    channelCreated[n-1,0] = 1
    
    #calculate theoretical bounds cricle for n = 4 and n = 5
    if n == 4: 
        
        x0 = 0 
        y0a= 0.5
        y0b = 1
        
        x1 = 1
        y1a= 0
        y1b = 1
        
        x2 = 1.2
        y2a= 0
        y2b = 1
        
    elif n ==5: 
        x0 = 0 
        y0a= 0.25
        y0b = 1
        
        x1 = 0.5
        y1a= 0
        y1b = 0.5
        
        x2 = 1
        y2a= 0
        y2b = 0        
    
    #calculate plot bounds 
    xMax = x2
    yMax = y0b*1.05
    
    #calculate step size for sweep
    xDelta = xMax/20.0
    yDelta = yMax/20.0    
    
    #(b,c) coordiniates of Nash equilibria
    bNash = []
    cNash = []
    
    #(b,c) coordiniates of non Nash equilibria
    bNoNash = []
    cNoNash = []
    
    #sweep coordinates
    for i in range(0,21): 
            for j in range(0,21): 
                
                b = xDelta*i 
                c= yDelta*j
                
                if j == 0: 
                    c = 0.001
                    
                #check if circle is a Nash eq
                temp = nashEquilibria(n,b,c)
                nash = temp.isNash(channelCreated)
                
                #save coordinates
                if nash == 0: 
                    bNoNash =np.append(bNoNash,b)
                    cNoNash =np.append(cNoNash,c)
                    
                else:
                    bNash =np.append(bNash,b)
                    cNash =np.append(cNash,c)
                    
                    
    #specify font 
    font = {'family' : 'serif',
            'weight' : 'normal',
            'size'   : 12}
   
    mpl.rc('font',**font)        

    #draw plot
    plt.figure(figsize=(3.5,3.5))   
    
    #theory area
    if n== 4: 
        plt.fill_between([x0,x1,x2], [y0a, y1a,y2a], [y0b, y1b,y2b],facecolor='#c8faa0', label='theory')
                                  
    else: 
        plt.fill_between([x0,x1,x2], [y0a, y1a,y2a], [y0b, y1b,y2b],facecolor='#c8faa0', label='theory')    
          
    #simulaiton results
    plt.scatter(bNash, cNash, c="green", alpha=0.5,s=10, label ="Nash equilibrium")
    plt.scatter(bNoNash, cNoNash, c="red", alpha=0.5,s=10, label ="no Nash equilibrium")
    
    #create legend
    ax = plt.gca()    
    handles, labels = ax.get_legend_handles_labels()
    lgd = ax.legend(handles, labels, bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=1, mode="expand", borderaxespad=0.)
    
    #axis labels
    plt.xlabel('b')
    plt.ylabel('c')
    
    #name of png
    namepng = "circle_n"+str(n)+".png"
    
    #export plot
    plt.savefig(namepng,dpi=200, bbox_extra_artists=(lgd,),bbox_inches='tight')
     
    #show plot
    plt.show()
    
    
def parameterSweepStar(n): 
    """
    This function performs a parameter sweep to see for which (b,c), the 
    star graph is a Nash equilibrium.
    
    :param n int: The number of players. 
    """
    
    #raise expection if n ist not an integer
    if isinstance(n, int) == 0: 
        raise Exception('n has to be integer')
        
    #ensure that the game has at least 2 players
    if n <= 1:
        raise Exception('n has to be at least 2')
    
    #initialize array indicating the created channel
    #Note: Here we create a star where one node connects to all others. However, 
    #all version are equivalent. 
    channelCreated = np.zeros((n,n))
    for i in range(1,n): 
        channelCreated[0, i] = 1
            
    #calculate theoretical bounds
    x0 = 0 
    y0 = 1
    x1 = 2/(float(n)-3)
    y1 = 0
    
    #calculate plot bounds 
    xMax = x1*1.05
    yMax = y0*1.05
    
    #calculate step size
    xDelta = xMax/20.0
    yDelta = yMax/20.0
    
    #(b,c) coordiniates of Nash equilibria
    bNash = []
    cNash = []
    
    #(b,c) coordiniates of non Nash equilibria
    bNoNash = []
    cNoNash = []
    
    #sweep coordinates
    for i in range(0,21): 
            for j in range(0,21): 
                
                b = xDelta*i 
                c= yDelta*j 
                
                if j == 0: 
                    c = 0.001
                    
                #check if star is a Nash eq
                temp = nashEquilibria(n,b,c)
                nash = temp.isNash(channelCreated)
                
                #save coordinates
                if nash == 0: 
                    bNoNash =np.append(bNoNash,b)
                    cNoNash =np.append(cNoNash,c)
                    
                else:
                    bNash =np.append(bNash,b)
                    cNash =np.append(cNash,c)
           
    #specify font
    font = {'family' : 'serif',
            'weight' : 'normal',
            'size'   : 12}
    
    mpl.rc('font',**font)
    
    #draw plot
    plt.figure(figsize=(3.5,3.5))
    
    #theory area
    plt.fill_between([x0,x1], [y0, y1],facecolor='#c8faa0', label='theory')
    
    #simulaiton results
    plt.scatter(bNash, cNash, c="green", alpha=0.5,s=10, label ="Nash equilibrium")
    plt.scatter(bNoNash, cNoNash, c="red", alpha=0.5,s=10, label ="no Nash equilibrium")
    
    #create legend
    ax = plt.gca()    
    handles, labels = ax.get_legend_handles_labels()
    lgd = ax.legend(handles, labels, bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=1, mode="expand", borderaxespad=0.)
  
    #axis lables
    plt.xlabel('b')
    plt.ylabel('c')
    
    #name of png
    namepng = "star_n"+str(n)+".png"
    
    #export plot
    plt.savefig(namepng,dpi=200, bbox_extra_artists=(lgd,),bbox_inches='tight')
    
    #show plot
    plt.show()

    
def parameterSweepCompleteGraph(n): 
    """
    This function performs a parameter sweep to see for which (b,c), the 
    complete graph is a Nash equilibrium.
    
    :param n int: The number of players. 
    """
    
    #raise expection if n is not an integer
    if isinstance(n, int) == 0: 
        raise Exception('n has to be integer')
        
    #ensure that the game has at least 2 players
    if n <= 1:
        raise Exception('n has to be at least 2')
        
    #initialize array indicating the created channel
    channelCreated = np.zeros((n,n))
    for i in range(0,n): 
        for j in range(0,n): 
            if i != j and i <j:  
                channelCreated[i, j] = 1            
    
    #calculate theoretical bounds
    x0 = 0 
    y0 = 1
    x1 = 2
    y1 = 1
    
    #calculate plot bounds 
    xMax = 2
    yMax = 2
    
    #calculate step size
    xDelta = xMax/20.0
    yDelta = yMax/20.0    
    
    #(b,c) coordiniates of Nash equilibria
    bNash = []
    cNash = []
    
    #(b,c) coordiniates of non Nash equilibria
    bNoNash = []
    cNoNash = []
    
    #sweep coordinates
    for i in range(0,21): 
            for j in range(0,21): 
                
                b = xDelta*i 
                c= yDelta*j 
                
                if j == 0: 
                    c = 0.001
                    
                #check if complete  graph is a Nash eq
                temp = nashEquilibria(n,b,c)
                nash = temp.isNash(channelCreated)
                
                #save coordinates
                if nash == 0: 
                    bNoNash =np.append(bNoNash,b)
                    cNoNash =np.append(cNoNash,c)
                    
                else:
                    bNash =np.append(bNash,b)
                    cNash =np.append(cNash,c)
                    
    #specify font
    font = {'family' : 'serif',
        'weight' : 'normal',
        'size'   : 12}    
    
    mpl.rc('font',**font)
    
    #draw plot
    plt.figure(figsize=(3.5,3.5))
    
    #theory area
    plt.fill_between([x0,x1], [y0, y1], [2,2],facecolor='#c8faa0', label='theory')
    
    #simulaiton results
    plt.scatter(bNash, cNash, c="green", alpha=0.5,s=10, label ="Nash equilibrium")
    plt.scatter(bNoNash, cNoNash, c="red", alpha=0.5,s=10, label ="no Nash equilibrium")
    
    #create legend
    ax = plt.gca()    
    handles, labels = ax.get_legend_handles_labels()
    lgd = ax.legend(handles, labels, bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=1, mode="expand", borderaxespad=0.)
    
    #axis labels
    plt.xlabel('b')
    plt.ylabel('c')
   
    #name of png
    namepng = "complete_n"+str(n)+".png"
    
    #export plot
    plt.savefig(namepng,dpi=200, bbox_extra_artists=(lgd,),bbox_inches='tight')
    
    #show plot
    plt.show()
    


def parameterSweepBipartite(n,r,s): 
    """
    This function performs a parameter sweep to see for which (b,c), the 
    complete bipartite graph K_{r,s} is a Nash equilibrium. Where r <= s and
    r>= 3. 
    
    :param n int: The number of players. 
    :param r int: The size of the smaller subset of K_{r,s}. 
    :param s int: The size of the larger subset of K_{r,s}. 
    """
    
    #raise expection if n is not an integer
    if isinstance(n, int) == 0: 
        raise Exception('n has to be integer')
    
    #ensure that the game has at least 2 players
    if n <= 1:
        raise Exception('n has to be at least 2')
        
    #raise expection if r is not an integer
    if isinstance(r, int) == 0: 
        raise Exception('r has to be integer')
        
    #ensure that r >= 3 for theoretical bounds
    if r < 3:
        raise Exception('r has to be at least 3')
        
    #raise expection if s is not an integer
    if isinstance(s, int) == 0: 
        raise Exception('s has to be integer')
    
    #ensure that s >= 3 for theoretical bounds
    if s < 3:
        raise Exception('s has to be at least 3')
        
    #raise expection if n != r + s is not an integer
    if r + s != n: 
        raise Exception('r and s need to sum to n')
    
    #raise expection if s < r
    if s < r:
        raise Exception('s cannot be smaller than r')
    
    #initialize array indicating the created channel
    #Note: Links go from the smaller to the bigger subset. As these bicliques 
    #are stable for a greater parameter combination. 
    channelCreated = np.zeros((n,n))
    for i in range(r,n): 
        for j in range(0,r):             
            channelCreated[j, i] = 1
            
    #avoid integer divisions
    r = float(r)    
    s = float(s) 
    
    #calculate theoretical bounds#
    x00 = 0 
    y00 = 1
    x01 = (r+1)/(s-2)
    y01 = 0
    
    xaTemp =1/(1/(s-(r-1))*( (s*(s-1))/r - ((r-2)*(r-1))/(s+1)))
    
    xbTemp =(r*(s-2))/(s*(s-1))
    
    if xaTemp >= xbTemp: 
        x10 = 0 
        y10 = 1
        x11 = 1/(1/(s-(r-1))*( (s*(s-1))/r - ((r-2)*(r-1))/(s+1)))
        y11 = 0
    
    else: 
    
        x10 = 0 
        y10 = 1
        x11 = (r*(s-2))/(s*(s-1))
        y11 = 0
    
    x20 = 0 
    y20 = (s-1)/(s+r-3)
    x21 =(r)/(s)
    y21 = 0
    
    points10 = (x10, y10)
    points11 = (x11, y11)
    points20 = (x20, y20)
    points21 = (x21, y21)

    xa,ya = line_intersection((points10,points11),(points20,points21))
    
    m = (y00-y01)/(x00-x01)
    b = (x00*y00 - x01*y00)/(x00-x01)
    
    y1 = m * xa + b 
    y2 = m * x21 + b 
    
    #(b,c) coordiniates of Nash equilibria
    bNash = []
    cNash = []
    
    #(b,c) coordiniates of non Nash equilibria
    bNoNash = []
    cNoNash = []
    
    #calculate plot bounds 
    xMax = x01*1.05
    yMax = y00*1.05
    
    #calculate step size
    xDelta = xMax/20.0
    yDelta = yMax/20.0
    
    #sweep coordinates
    for i in range(0,21): 
            for j in range(0,21): 
                
                b = xDelta*i 
                c= yDelta*j 
                
                if j == 0: 
                    c = 0.001
                    
                #check if K_{r,s} is a Nash eq
                temp = nashEquilibria(n,b,c)
                nash = temp.isNash(channelCreated)
                
                #save coordinates
                if nash == 0: 
                    bNoNash =np.append(bNoNash,b)
                    cNoNash =np.append(cNoNash,c)
                    
                else:
                    bNash =np.append(bNash,b)
                    cNash =np.append(cNash,c)
           
    #specify font
    font = {'family' : 'serif',
        'weight' : 'normal',
        'size'   : 12}
    
    mpl.rc('font',**font)
    
    #draw plot
    plt.figure(figsize=(3.5,3.5))    
    
    #theory area
    plt.fill_between([x00,xa , x21,x01], [y00, y1, y2  ,y01] , [y00,ya, y21  ,y01], 
                     facecolor='#c8faa0', label='theory')
    
    #simulaiton results
    plt.scatter(bNash, cNash, c="green", alpha=0.5,s=10, label ="Nash equilibrium")
    plt.scatter(bNoNash, cNoNash, c="red", alpha=0.5,s=10, label ="no Nash equilibrium")
    
    #create legend
    ax = plt.gca()    
    handles, labels = ax.get_legend_handles_labels()
    lgd = ax.legend(handles, labels, bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=1, mode="expand", borderaxespad=0.)

    #axis labels
    plt.xlabel('b')
    plt.ylabel('c')
    
    #name of png
    namepng = "bipartite_n"+str(n)+"r"+str(int(r))+"s"+str(int(s))+".png"
    
    #export plot
    plt.savefig(namepng,dpi=200, bbox_extra_artists=(lgd,),bbox_inches='tight')    
    
    #show plot
    plt.show()


def generalParameterSweep(n, bMin, bMax, cMin, cMax, bSteps, cSteps, channelCreated):
    """
    This function performs a parameter sweep to see for which (b,c), 
    channelCreated is a Nash equilibrium.
    
    :param n int: The number of players. 
    :param bMin float: The minimum b for parameter sweep. 
    :param bMax float: The maximum b for parameter sweep. 
    :param cMin float: The minimum c for parameter sweep. 
    :param cMax float: The maximum c for parameter sweep. 
    :param bSteps int: The number of steps for b. 
    :param cSteps int: The number of steps for c. 
    :param channelCreated bool: Array indicating which channels are created.
    """
    
    #raise expection if n is not an integer
    if isinstance(n, int) == 0: 
        raise Exception('n has to be integer')
        
    #ensure that the game has at least 2 players
    if n <= 1:
        raise Exception('n has to be at least 2')
        
    #raise expection if bMin > bMax 
    if bMin > bMax: 
        raise Exception('bMin cannot be bigger than bMax')
        
    #raise expection if bSteps is not an integer
    if isinstance(bSteps, int) == 0: 
        raise Exception('bSteps has to be integer')
    
    #raise expection if cMin > cMax i
    if cMin > cMax: 
        raise Exception('cMin cannot be bigger than cMax')    
        
    #raise expection if cSteps is not an integer
    if isinstance(cSteps, int) == 0: 
        raise Exception('cSteps has to be integer')
    
    #calculate step size
    bDelta = (bMax-bMin)/float(bSteps-1)
    cDelta = (cMax-cMin)/float(cSteps-1)
    
    #(b,c) coordiniates of Nash equilibria
    bNash = []
    cNash = []
    
    #(b,c) coordiniates of non Nash equilibria
    bNoNash = []
    cNoNash = []
    
    #sweep coordinates
    for i in range(0,bSteps): 
            for j in range(0,cSteps): 
                
                b = bDelta*i 
                c= cDelta*j 
                
                if j == 0: 
                    c = 0.001
                    
                #check if star is a Nash eq
                temp = nashEquilibria(n,b,c)
                nash = temp.isNash(channelCreated)
                
                #save coordinates
                if nash == 0: 
                    bNoNash =np.append(bNoNash,b)
                    cNoNash =np.append(cNoNash,c)
                    
                else:
                    bNash =np.append(bNash,b)
                    cNash =np.append(cNash,c)
           
    #specify font
    font = {'family' : 'serif',
            'weight' : 'normal',
            'size'   : 12}
    
    mpl.rc('font',**font)
    
    #draw plot
    plt.figure(figsize=(3.5,3.5))
    
    #simulaiton results
    plt.scatter(bNash, cNash, c="green", alpha=0.5,s=10, label ="Nash equilibrium")
    plt.scatter(bNoNash, cNoNash, c="red", alpha=0.5,s=10, label ="no Nash equilibrium")
    
    #create legend
    ax = plt.gca()    
    handles, labels = ax.get_legend_handles_labels()
    lgd = ax.legend(handles, labels, bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=1, mode="expand", borderaxespad=0.)
  
    #axis lables
    plt.xlabel('b')
    plt.ylabel('c')
    
    #name of png
    namepng = "parameterSweep_n"+str(n)+".png"
    
    #export plot
    plt.savefig(namepng,dpi=200, bbox_extra_artists=(lgd,),bbox_inches='tight')
    
    #show plot
    plt.show()

  
    